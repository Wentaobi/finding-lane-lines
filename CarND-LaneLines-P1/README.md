# **Finding Lane Lines on the Road** 
[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

<img src="examples/laneLines_thirdPass.jpg" width="480" alt="Combined Image" />

Overview
---

When we drive, we use our eyes to decide where to go.  The lines on the road that show us where the lanes are act as our constant reference for where to steer the vehicle.  Naturally, one of the first things we would like to do in developing a self-driving car is to automatically detect lane lines using an algorithm.

In this project you will detect lane lines in images using Python and OpenCV.  OpenCV means "Open-Source Computer Vision", which is a package that has many useful tools for analyzing images.  

To complete the project, two files will be submitted: a file containing project code and a file containing a brief write up explaining your solution. We have included template files to be used both for the [code](https://github.com/udacity/CarND-LaneLines-P1/blob/master/P1.ipynb) and the [writeup](https://github.com/udacity/CarND-LaneLines-P1/blob/master/writeup_template.md).The code file is called P1.ipynb and the writeup template is writeup_template.md 

To meet specifications in the project, take a look at the requirements in the [project rubric](https://review.udacity.com/#!/rubrics/322/view)


Creating a Great Writeup
---
For this project, a great writeup should provide a detailed response to the "Reflection" section of the [project rubric](https://review.udacity.com/#!/rubrics/322/view). There are three parts to the reflection:

1. Describe the pipeline

	In the white lane detection, My pipeline function did almost like this:
	input: img, vertices, hough lines threshold angle.
	output: result img
	I convert the original image to the gray scale image. Then I use 3*3 gaussian blur filter to make it smooth, later use canny operator to detect edges, the threshold are 10 and 180, which are gradients. and also, I need to choose ROI, interested area to plot. right now it's egde image so I can use hough lines detection to select lines and draw them based on original image.

	In the yellow line detection, I add white pixel range and yellow range in hsv domain image, to pick out these pixels and then add them in one image with yellow and white line, the other part are pretty much same as white line detection.

2. Identify any shortcomings

	The pipeline function does not work with every frame of precise lane detection. And I spend much time on daw lines function, and get some help from nanodegree forum and github code sharing. I put the link that the code I used. And I make sense of code before I used it.
    
    Finally, I have some video issue. The outout video always play only one frame back.

3. Suggest possible improvements

	Pipeline works for the most of lane lines detection. 
    

